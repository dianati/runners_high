package org.dianati.test.beans;

import org.dianati.runtime.api.annotation.ExportService;

@ExportService(name = "test2")
public class TestBeanTwo {

    private final TestBean testBean;

    public TestBeanTwo(TestBean testBean) {
        this.testBean = testBean;
    }

    public void print() {
        testBean.print();
    }
}
