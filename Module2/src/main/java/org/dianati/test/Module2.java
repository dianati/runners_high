package org.dianati.test;

import org.dianati.runtime.api.annotation.*;
import org.dianati.runtime.api.common.StateChangeEvent;
import org.dianati.test.beans.TestBeanTwo;

@RunnersModule(name = "Module_2")
public class Module2 {

    volatile boolean run = true;
    private final Object lock = new Object();
    private final TestBeanTwo testBeanTwo;

    public Module2(TestBeanTwo testBeanTwo) {this.testBeanTwo = testBeanTwo;}

    @Start
    public void start() {
        synchronized (lock) {
            while (run) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ignore) {
                    System.out.println("FU!");
                }
                System.out.println("Module_2 does some weird stuff!");
                testBeanTwo.print();
            }
        }
    }

    @Stop
    public void stop() {
        System.out.println("Module_2 will shut down now!");
        run = false;
        synchronized (lock) {
            System.out.println("Module_2 finished!!");
        }
    }

    @Observes
    public void observer(StateChangeEvent event) {
        System.out.printf("A state changed from %s to %s in module %s\n", event.getPrevState(), event.getActState(), event.getModule());
    }
}
