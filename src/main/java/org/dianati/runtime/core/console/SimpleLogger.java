package org.dianati.runtime.core.console;

import org.dianati.runtime.api.annotation.ExportService;
import org.dianati.runtime.api.common.Logger;

@ExportService(name = "Logger")
public class SimpleLogger implements Logger {
    @Override
    public void log(Level level, String message) {
        System.out.println(String.format("[%-7s] %s", level, message));
    }
}
