package org.dianati.runtime.core.console;

import org.dianati.runtime.core.module.ModuleManager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

@Component
//@Profile("WithConsole")
public class RunnersConsole implements CommandLineRunner {

    private final InputStream in;
    private final OutputStream out;
    private final ModuleManager manager;
    private final HashMap<String, Command> bindings = new HashMap<>();
    private static final String EXIT = "exit";
    private static final String LIST_COMMANDS = "lsc";
    private boolean run = false;
    private String prefix = "";

    public RunnersConsole(ModuleManager manager, ConfigurableApplicationContext context) {
        this.manager = manager;
        this.in = System.in;
        this.out = System.out;
        bindings.put(EXIT, new Command(EXIT, 0, (args) -> {
            run = false;
            manager.stopAll();
        }));
        bindings.put(LIST_COMMANDS, new Command(LIST_COMMANDS, 0, args -> {
            final PrintWriter writer = new PrintWriter(out);
            writer.println(prefix + "available commands:");
            bindings.values().forEach(
                    v -> writer.printf("%s> %-10s - args %2d - %s\n", prefix, v.getId(), v.getArgs(), v.getDescription()));
            writer.flush();
        }));

        createBinding(new Command("deploy", 1
                , arg -> manager.deployModule(arg[0])));
        createBinding(new Command("start", 1
                , arg -> manager.startModule(Integer.parseInt(arg[0]))));
        createBinding(new Command("stop", 1
                , arg -> manager.stopModule(Integer.parseInt(arg[0]))));
        createBinding(new Command("undeploy", 1
                , arg -> manager.undeployModule(Integer.parseInt(arg[0]))));
        createBinding(new Command("list", 0
                , arg -> manager.getModules().forEach((k, v) -> System.out.printf("id %2d - %s - %s\n", k, v.getName(), v.getState()))));
    }

    /**
     * @param command the command, 'exit' can not be overwritten
     * @return the previous value associated with command, or null
     */
    public Command createBinding(Command command) {
        Objects.requireNonNull(command);
        if (command.getId().equals(EXIT)) return null;
        return bindings.put(command.getId(), command);
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param command the command to remove
     * @return the previous value associated
     */
    public Command removeBinding(String command) {
        if (command.equals(EXIT)) return null;
        return bindings.remove(command);
    }

    /**
     * @param prefix the prefix will be written in front of every line
     */
    public void setOutputPrefix(String prefix) {
        this.prefix = prefix + " ";
    }

    @Override
    public void run(String... args) {
        final Scanner scanner = new Scanner(in);
        final PrintWriter writer = new PrintWriter(out);
        run = true;
        while (run) {
            writer.print(prefix + "$command: ");
            writer.flush();
            try {
                final String line = scanner.nextLine();
                final String[] split = line.split(" ");
                final Command command = bindings.get(split[0]);
                if (command == null) {
                    writer.printf(prefix + "%s > could not find command!\n", split[0]);
                } else if (command.getArgs() != split.length - 1) {
                    writer.printf(prefix + "%s > wrong number of arguments. Allowed are %d!\n", split[0], command.getArgs());
                } else if (split.length > 1) {
                    command.getConsumer().consume(Arrays.copyOfRange(split, 1, split.length));
                } else {
                    command.getConsumer().consume();
                }
            } catch (Exception exception) {
                writer.printf(prefix + "ERROR> execution error: %s!\n", exception.getLocalizedMessage());
            }
            writer.flush();
        }
        writer.println(prefix + "exiting!");
        writer.flush(); // FIXME might not be usefull here
    }
}

