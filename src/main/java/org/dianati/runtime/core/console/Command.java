package org.dianati.runtime.core.console;

import org.dianati.runtime.core.interfaces.ArgConsumer;

import java.util.Objects;

public final class Command {
    private final String id;
    private final int args;
    private final ArgConsumer consumer;
    private final String description;

    public Command(String id, int args, ArgConsumer consumer, String description) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(consumer);
        this.id = id;
        this.args = args;
        this.consumer = consumer;
        this.description = description;
    }

    public Command(String id, int args, ArgConsumer consumer) {
        this(id, args, consumer, null);
    }

    public String getId() {
        return id;
    }

    public int getArgs() {
        return args;
    }

    public ArgConsumer getConsumer() {
        return consumer;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Command command = (Command) o;
        return Objects.equals(id, command.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Command{" +
                "id='" + id + '\'' +
                ", args=" + args +
                ", description='" + description + '\'' +
                '}';
    }
}
