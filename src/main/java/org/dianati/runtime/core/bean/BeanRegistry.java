package org.dianati.runtime.core.bean;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Qualifier("MainBeanRegistry")
public class BeanRegistry {
    private final List<BeanWrapper> beans = Collections.synchronizedList(new ArrayList<BeanWrapper>());

    public List<BeanWrapper> getBeans() {
        return beans;
    }

    public void addBean(BeanWrapper beanWrapper) {
        beans.add(beanWrapper);
    }

    public <T> T getBean(String name, Class<T> requiredType) throws Exception {
        throw new Exception("Not implemented!");
    }

    @SuppressWarnings("unchecked")
    public <T> T getBean(Class<T> requiredType) throws IllegalAccessException, InvocationTargetException, InstantiationException, BeanException, ClassNotFoundException {
        final List<BeanWrapper> beanWrappers = searchForBeans(requiredType);
        if (beanWrappers.size() == 0) throw new BeanException("No bean found! Type " + requiredType);
        if (beanWrappers.size() > 1) throw new BeanException("Duplicate bean found! Type " + requiredType);
        return (T) beanWrappers.get(0).getBean();
    }

    public void clear() {
        beans.clear();
    }

    public void removeAllOf(int moduleId) {
        beans.removeIf(m -> m.getModuleId() == moduleId);
    }

    private List<BeanWrapper> searchForBeans(Class<?> requiredType) {
        //rename getter? :D
        List<BeanWrapper> list = new ArrayList<>();
        for (BeanWrapper b : beans) {
            if (!b.isPrivate() && requiredType.isAssignableFrom(b.getClazz())) {
                list.add(b);
            }
        }
        return list;
    }
}
