package org.dianati.runtime.core.bean;

import org.dianati.runtime.api.annotation.ExportService;
import org.dianati.runtime.api.annotation.Inject;
import org.dianati.runtime.api.common.Event;
import org.dianati.runtime.core.annotation.AnnotationProcessor;
import org.dianati.runtime.core.utility.Assert;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

@Component
@Qualifier(value = "MainBeanFactory")
public class BeanFactory {

    private final BeanRegistry beanRegistry;
    private final DirtyEventHandler eventHandler;

    public BeanFactory(BeanRegistry beanRegistry, DirtyEventHandler dh) {
        this.eventHandler = dh;
        this.beanRegistry = beanRegistry;
    }

    public <T> Object newInstanceOf(Class<T> clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, BeanException {
        //search for a Constructor
        final Constructor<?>[] constructors = clazz.getConstructors();
        final Constructor<?> constructor = constructors[0];

        Assert.sizeIs(constructors, 1, "Only a single public constructor is allowed!");

        if (constructor.getParameterCount() == 0) {
            return injectFields(constructor.newInstance());
        } else {
            Object[] args = new Object[constructor.getParameterCount()];
            for (int i = 0; i < args.length; i++) {

                if (constructor.getParameterTypes()[i].equals(Event.class)) {
                    final ParameterizedType pType = (ParameterizedType) constructor.getGenericParameterTypes()[i];
                    final Class<?> payload;
                    payload = BeanFactory.class.getClassLoader().loadClass(pType.getActualTypeArguments()[0].getTypeName());
                    final Event<?> event = eventHandler.registerNotifier(payload);
                    args[i] = event;
                } else {
                    args[i] = beanRegistry.getBean(constructor.getParameterTypes()[i]);
                }
            }

            return injectFields(constructor.newInstance(args));
        }
    }

    private Object injectFields(Object object) throws IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, BeanException {
        for (Field field : object.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Inject.class)) {
                if (field.getType().equals(Event.class)) {
                    ParameterizedType pType = (ParameterizedType) field.getGenericType();
                    final Class<?> payload = BeanFactory.class.getClassLoader().loadClass(pType.getActualTypeArguments()[0].getTypeName());
                    final Event<?> event = eventHandler.registerNotifier(payload);
                    if (field.isAccessible()) {
                        field.set(object, event);
                    } else {
                        field.setAccessible(true);
                        field.set(object, event);
                        field.setAccessible(false);
                    }
                } else {
                    if (field.isAccessible()) {
                        field.set(object, beanRegistry.getBean(field.getType()));
                    } else {
                        field.setAccessible(true);
                        field.set(object, beanRegistry.getBean(field.getType()));
                        field.setAccessible(false);
                    }
                }
            }
        }
        return object;
    }

    public void addBean(Class<?> clazz, int moduleId) throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException, BeanException {
        if (AnnotationProcessor.isAnnotatedWith(clazz, ExportService.class)) {
            final ExportService annotation = clazz.getAnnotation(ExportService.class);
            final boolean singleton = annotation.singleton();
            final String name = annotation.name();

            if (singleton) {
                this.beanRegistry.addBean(new BeanWrapper(clazz, newInstanceOf(clazz), null, true, name, moduleId));
            } else {
                this.beanRegistry.addBean(new BeanWrapper(clazz, null, this, false, name, moduleId));
            }
        } else
            throw new RuntimeException("Unexpected annotation!"); //TODO logging
    }
}
