package org.dianati.runtime.core.bean;

public class BeanException extends Exception {
    public BeanException(String text){
        super(text);
    }
}
