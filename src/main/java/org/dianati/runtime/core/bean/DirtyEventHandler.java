package org.dianati.runtime.core.bean;

import org.dianati.runtime.api.common.Event;
import org.dianati.runtime.core.module.ModuleEntry;
import org.dianati.runtime.core.module.ModuleManager;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class DirtyEventHandler { //this is a very uneffient and ugly way to do it, i just didn't want to waste my time...

    private final ModuleManager moduleManager;

    public DirtyEventHandler(@Lazy ModuleManager manager) {
        moduleManager = manager;
    }

    public <T> Event<T> registerNotifier(Class<T> payloadType) {
        final Event<T> event = payload -> {
            for (ModuleEntry entry : moduleManager.getModules().values()) {
                if (entry.getInstance() != null) { //fixme should me entry.getState == Active
                    final Method observer = entry.getObserves().get(payload.getClass());
                    if (observer != null) {
                        try {
                            observer.invoke(entry.getInstance(), payload);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace(); //TODO error handling
                        }
                    }
                }
            }
        };
        return event;
    }
}
