package org.dianati.runtime.core.bean;

import java.lang.reflect.InvocationTargetException;

public class BeanWrapper {

    private final Class<?> clazz;
    private final boolean singleton;
    private final Object instance;
    private final BeanFactory factory;
    private final String name;
    private final boolean priv = false;
    private final int moduleId;

    public BeanWrapper(Class<?> clazz, Object instance, BeanFactory factory, boolean singleton, String name, int moduleId) {
        this.factory = factory;
        this.singleton = singleton;
        this.clazz = clazz;
        this.instance = instance;
        this.name = name;
        this.moduleId = moduleId;
    }

    public Object getBean() throws IllegalAccessException, InstantiationException, InvocationTargetException, BeanException, ClassNotFoundException {
        if (singleton) {
            return instance;
        } else {
            return factory.newInstanceOf(clazz);
        }
    }

    public String getName() {
        return name;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public boolean isPrivate() {
        return priv;
    }

    public boolean isSingleton() {
        return singleton;
    }

    public int getModuleId() {
        return moduleId;
    }
}
