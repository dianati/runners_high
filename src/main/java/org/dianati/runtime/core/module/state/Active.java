package org.dianati.runtime.core.module.state;

import org.dianati.runtime.core.module.ModuleEntry;
import org.dianati.runtime.core.module.ModuleManager;

public class Active extends ModuleState {

    public Active(ModuleEntry module, ModuleManager manager) {super(module, manager);}

    @Override
    public void stop() { //TODO in future references have to be checke to shutdown in the proper order this could also be done even before this method was called
       manager.deactivate(module);
    }
}
