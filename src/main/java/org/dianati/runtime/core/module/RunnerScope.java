package org.dianati.runtime.core.module;

public enum RunnerScope {
    TEST,
    PRODUCTION,
    INSPECTION,
    MAINTANANCE
}
