package org.dianati.runtime.core.module.state;

import org.dianati.runtime.core.module.ModuleEntry;
import org.dianati.runtime.core.module.ModuleException;
import org.dianati.runtime.core.module.ModuleManager;

public abstract class ModuleState {

    protected final ModuleEntry module; //state context
    protected final ModuleManager manager; //state context

    protected ModuleState(ModuleEntry module, ModuleManager manager) {
        this.module = module;
        this.manager = manager;
    }

    public void start() throws ModuleException {}

    public void stop() throws ModuleException {}

    @Override
    public String toString() {
        return this.getClass().getSimpleName().toUpperCase();
    }
}
