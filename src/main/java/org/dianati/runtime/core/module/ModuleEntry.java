package org.dianati.runtime.core.module;

import org.dianati.runtime.core.module.state.Deployed;
import org.dianati.runtime.core.module.state.ModuleState;

import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

public class ModuleEntry {

    private int id;
    private final String name;
    private final Method entryPoint;
    private final Method endPoint;
    private final Class entryClass;
    private final URLClassLoader classloader;

    private Path location;

    private ArrayList<Class> beans;

    private Object instance = null;

    private final HashMap<Class<?>, Method> observes = new HashMap<>();

    private ModuleState state; //FIXME vorübergehend
//    private final HashMap<Integer, Module> imports; //TODO import/export packages

    public ModuleEntry(String name, Class entryClass, Method entryPoint, Method endPoint, URLClassLoader classLoader, ModuleManager manager) {
        this.name = name;
        this.endPoint = endPoint;
        this.entryClass = entryClass;
        this.entryPoint = entryPoint;
        this.classloader = classLoader;
        state = new Deployed(this, manager);
    }

    public HashMap<Class<?>, Method> getObserves() {
        return observes;
    }

    public Path getLocation() {
        return location;
    }

    public void setLocation(Path location) {
        this.location = location;
    }

    public void setState(ModuleState state) {
        this.state = state;
    }

    public ModuleState getState() {
        return state;
    }

    public URLClassLoader getClassloader() { //TODO auslagern
        return classloader;
    }

    public void start() throws ModuleException {
        state.start();
    }

    public void stop() throws ModuleException {
        state.stop();
    }

    public void setId(int id) {
        this.id = id;
    }

    public Method getEntryPoint() {
        return entryPoint;
    }

    public Class getEntryClass() {
        return entryClass;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Method getEndPoint() {
        return endPoint;
    }

    public Object getInstance() {
        return instance;
    }

    public void setInstance(Object instance) {
        this.instance = instance;
    }

    public ArrayList<Class> getBeans() {
        return beans;
    }

    public void setBeans(ArrayList<Class> beans) {
        this.beans = beans;
    }
}
