package org.dianati.runtime.core.module.state;

import org.dianati.runtime.core.bean.BeanException;
import org.dianati.runtime.core.module.ModuleEntry;
import org.dianati.runtime.core.module.ModuleException;
import org.dianati.runtime.core.module.ModuleManager;

public class Deployed extends ModuleState {

    public Deployed(ModuleEntry module, ModuleManager manager) {super(module, manager);}

    @Override
    public void start() throws ModuleException {
        try {
            manager.activate(module);
        } catch (BeanException e) {
            throw new RuntimeException(e);
        }
        module.setState(new Active(module, manager));
    }
}
