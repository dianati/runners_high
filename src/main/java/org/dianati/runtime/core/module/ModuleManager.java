package org.dianati.runtime.core.module;

import org.dianati.runtime.api.annotation.*;
import org.dianati.runtime.core.annotation.AnnotationProcessor;
import org.dianati.runtime.core.bean.BeanException;
import org.dianati.runtime.core.bean.BeanFactory;
import org.dianati.runtime.core.bean.BeanRegistry;
import org.dianati.runtime.core.console.SimpleLogger;
import org.dianati.runtime.core.module.state.Deployed;
import org.dianati.runtime.core.reflection.ClassFinder;
import org.dianati.runtime.core.reflection.ModuleClassloader;
import org.dianati.runtime.core.utility.Assert;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

@Component
@Qualifier(value = "ModuleManager")
public class ModuleManager { //TODO this is a mess... clean this up pls!!! ALso this is not threadsafe

    public static int NO_MODULE_ID = -1;

    private final Map<Integer, ModuleEntry> modules = Collections.synchronizedMap(new HashMap<Integer, ModuleEntry>()); //FIXME no one else should ever be able do take a reference of "module" (so it can be gced)
    private final AtomicInteger moduleCounter = new AtomicInteger(0); // FIXME: 09.03.2018 Make a real counter
    private final BeanFactory beanFactory;
    private final BeanRegistry beanRegistry;
    private final Executor executor;

    private final Path configFile = Paths.get("rhconfig");

    public ModuleManager(
            BeanFactory beanFactory,
            BeanRegistry beanRegistry,
            @Qualifier(value = "MainExecutioner") Executor executor) {
        this.beanRegistry = beanRegistry;
        this.executor = executor;
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    void init() {
        try { // FIXME: 09.03.2018 this should also be done with a simple annotation
            if (Files.exists(configFile))
                loadConfig();
            beanFactory.addBean(SimpleLogger.class, NO_MODULE_ID);
        } catch (Exception e) {
            e.printStackTrace(); //TODO logging
        }
    }

    private void loadConfig() throws IOException {
        for (String s : Files.readAllLines(configFile)) {
            deployModule(s);
        }
    }

    private void writeStateToConfig() { // TODO: 09.03.2018 Replace this with a better soloution that uses a DB
        final List<String> lines = modules.entrySet().stream()
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .map(s -> s.getValue().getLocation().toString())
                .collect(Collectors.toList());
        try {
            Files.write(configFile, lines, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException ignore) {} //FIXME
    }

    public Object newInstanceOf(Class<?> type) throws IllegalAccessException, InstantiationException, InvocationTargetException, BeanException, ClassNotFoundException {
        return beanFactory.newInstanceOf(type);
    }

    public void deployModule(String path) throws IOException {
        deployModule(new File(path));
    }

    public void deployModule(File path) throws IOException {
        final JarFile jar = new JarFile(path); //test if its a jar
//        JarEntry entry = jar.getJarEntry("META-INF"); //could do some stuff with jar entry

        final URL jarUrl = path.toURI().toURL();
        final ModuleClassloader importHandler = new ModuleClassloader(ModuleManager.class.getClassLoader());
        final URLClassLoader classloader = new URLClassLoader(new URL[]{jarUrl}, importHandler); //FIXME do not use module managers classloader ?

        final ArrayList<Class> classes = new ArrayList<>(10);
        final ArrayList<Class> beans = new ArrayList<>();
        ClassFinder.forEachClassIn(jar, classloader, c -> {
            if (c.isAnnotationPresent(RunnersModule.class)) {
                final Import imp = (Import) c.getAnnotation(Import.class);
                if (imp != null) {
                    for (String module : imp.modules()) {
                        final ClassLoader externalLoader = modules.values().stream()
                                .filter(m -> m.getName().equals(module))
                                .findAny()
                                .orElseThrow(() -> new RuntimeException("Could not find required import: " + module)) //little bit of a hack
                                .getClassloader();
                        importHandler.addClassloader(externalLoader);
                    }
                }
                classes.add(c);
            } else if (c.isAnnotationPresent(ExportService.class)) {
                beans.add(c);
            } else if (c.isInterface() && c.getSimpleName().equals("package-info")) {
                // TODO: 02.04.2018 Knu  implement package privacy, so that each module can chose the visability of their packages
            }
            return true;
        });

        Assert.sizeIs(classes, 1, "Only one '@Module' annotation allowed per jar!");

        final Class mainClass = classes.get(0);
        final String name = ((RunnersModule) mainClass.getAnnotation(RunnersModule.class)).name();

        final List<Method> startMethods = AnnotationProcessor.getMethodByAnnotation(mainClass, Start.class);
        Assert.sizeIs(startMethods, 1, "Only one '@Start' annotation allowed! ");

        final List<Method> stopMethods = AnnotationProcessor.getMethodByAnnotation(mainClass, Stop.class);
        Assert.sizeIs(startMethods, 1, "Only one '@Stop' annotation allowed! ");

        final List<Method> observers = AnnotationProcessor.getMethodByAnnotation(mainClass, Observes.class);

        //check for imports/exports


        final ModuleEntry newModuleEntry = new ModuleEntry(name, mainClass, startMethods.get(0), stopMethods.get(0), classloader, this);

        newModuleEntry.setBeans(beans);

        for (Method m : observers) {
            final Class<?>[] types = m.getParameterTypes();
            Assert.sizeIs(types, 1, "@Observer methods must have one parameter!");
            newModuleEntry.getObserves().put(types[0], m);
        }

        newModuleEntry.setLocation(path.toPath());
        deployModule(newModuleEntry);
        writeStateToConfig();
    }

    public void deployModule(ModuleEntry moduleEntry) {
        moduleEntry.setId(moduleCounter.incrementAndGet());
        modules.put(moduleCounter.get(), moduleEntry);
    }

    public void startModule(int id) throws ModuleException {
        final ModuleEntry moduleEntry;
        if ((moduleEntry = modules.get(id)) != null) {
            moduleEntry.start();
        }
    }

    public void undeployModule(int id) throws ModuleException {
        final ModuleEntry moduleEntry;
        if ((moduleEntry = modules.get(id)) != null) {
            moduleEntry.stop();
            try {
                moduleEntry.getClassloader().close();
            } catch (IOException e) {
                throw new ModuleException("Could not release the Jarfile");
            }
            modules.remove(id);
            System.gc(); //TODO maybe, maybe not... sometimes the jvm just doesn't release the file
        }
        writeStateToConfig();
    }

    public void stopModule(int id) throws ModuleException {
        final ModuleEntry moduleEntry;
        if ((moduleEntry = modules.get(id)) != null) {
            moduleEntry.stop();
        }
    }

    //should name this "stop runtime" :D
    public void stopAll() {
        for (int key : new ArrayList<>(modules.keySet())) {
            try {
                undeployModule(key);
            } catch (ModuleException ex) {
                //TODO logging
            }
        }
    }

    public Map<Integer, ModuleEntry> getModules() {
        return modules;
    }

    public void activate(ModuleEntry module) throws ModuleException, BeanException {
        //Don't use an Exception as control flow -.-
        final ArrayList<Class> beans = new ArrayList<>(module.getBeans());
        int counter = 0;
        int index = 0;
        while (beans.size() > 0) {
            if (counter > beans.size() * beans.size())
                throw new ModuleException("Could not resolve all dependencies for given beans");
            try {
                beanFactory.addBean(beans.get(index), module.getId());
                beans.remove(index);
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException | ClassNotFoundException e) {
                throw new ModuleException("Could not create the bean class for module" + module.getId());
            } catch (BeanException e) {
                index++;
                if (index == beans.size()) index = 0;
            }
            counter++;
        }

        if (module.getEndPoint() != null) {
            try {
                module.setInstance(newInstanceOf(module.getEntryClass()));
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException | ClassNotFoundException e) {
                throw new ModuleException("Could not create the entry class for module" + module.getId());
            }
            executor.execute(() -> {
                try {
                    module.getEntryPoint().invoke(module.getInstance());
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void deactivate(ModuleEntry module) {
        if (module.getEndPoint() != null) {
            try {
                module.getEndPoint().invoke(module.getInstance());
            } catch (IllegalAccessException | InvocationTargetException e) { //TODO logging
                e.printStackTrace();
            }
        }
        beanRegistry.removeAllOf(module.getId());
        module.setInstance(null);
        module.setState(new Deployed(module, this));
    }
}
