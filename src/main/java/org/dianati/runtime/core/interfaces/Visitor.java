package org.dianati.runtime.core.interfaces;

@FunctionalInterface
public interface Visitor<T> {

    /**
     * @return {@code true} if the algorithm should visit more results,
     * {@code false} if it should terminate now.
     */
    boolean visit(T t);

}
