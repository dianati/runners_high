package org.dianati.runtime.core.interfaces;

@FunctionalInterface
public interface ArgConsumer {
    void consume(String... args) throws Exception;
}
