package org.dianati.runtime.core.utility;

import java.util.Collection;

public class Assert {

    private static final String MESSAGE_TEXT = "<Expected '%s', Actual '%s'>";

    public static void notNull(Object object, String message) {
        if (object == null)
            throw new AssertionException("Expected object to be not null: " + message);
    }

    public static void sizeIs(Collection<?> collection, int size, String message) {
        if (collection.size() != size)
            throw new AssertionException(String.format(MESSAGE_TEXT, size, collection.size()) + ": " + message);
    }

    public static <T> void sizeIs(T[] array, int size, String message) {
        if (array.length != size)
            throw new AssertionException(String.format(MESSAGE_TEXT, size, array.length) + ": " + message);
    }

    public static void lengthIs(String string, int size, String message) {
        if (string.length() == size)
            throw new AssertionException(String.format(MESSAGE_TEXT, size, string.length()) + ": " + message);
    }

    public static void isInstanceOf(Object a, Object b) {
        if (!a.getClass().isInstance(b)) {
            throw new AssertionException(String.format(MESSAGE_TEXT, "true", "false"));
        }
    }
}
