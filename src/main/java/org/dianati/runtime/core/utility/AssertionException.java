package org.dianati.runtime.core.utility;

public class AssertionException extends RuntimeException {
    public AssertionException(String message) {
        super(message);
    }
}
