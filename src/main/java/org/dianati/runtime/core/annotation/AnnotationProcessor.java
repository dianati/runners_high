package org.dianati.runtime.core.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AnnotationProcessor {

    public static <T extends Annotation> boolean isAnnotatedWith(Class clazz, Class<T> annotation) {
        return clazz.getAnnotation(annotation) != null;
    }

    public static <T extends Annotation> List<Method> getMethodByAnnotation(Class clazz, Class<T> annotation) {
        final ArrayList<Method> methods = new ArrayList<>();
        for (Method method : clazz.getMethods()) {
            if (method.getAnnotation(annotation) != null) {
                methods.add(method);
            }
        }
        return methods;
    }
}
