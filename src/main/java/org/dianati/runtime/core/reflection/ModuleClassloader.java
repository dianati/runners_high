package org.dianati.runtime.core.reflection;

import java.util.ArrayList;

public class ModuleClassloader extends ClassLoader {

    private ArrayList<ClassLoader> externalClasses = new ArrayList<>();

    public ModuleClassloader(ClassLoader parent) {
        super(parent);
    }

    public void addClassloader(ClassLoader loader) {
        externalClasses.add(loader);
    }

    @Override
    public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        ClassNotFoundException exception;
        try {
            return super.loadClass(name, resolve);
        } catch (ClassNotFoundException e) {
            exception = e;
            for (ClassLoader loader : externalClasses) {
                try {
                    return loader.loadClass(name);
                } catch (ClassNotFoundException ignore) {}
            }
        }
        throw new ClassNotFoundException("Could not find class!", exception);
    }
}
