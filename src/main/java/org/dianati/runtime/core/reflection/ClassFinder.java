package org.dianati.runtime.core.reflection;


import org.dianati.runtime.core.interfaces.Visitor;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassFinder {
    public static void findClasses(Visitor<Class> visitor) {
        final String seperator = FileSystems.getDefault().getSeparator();
        final String classpath = System.getProperty("java.class.path");
        final String[] paths = classpath.split(System.getProperty("path.separator"));

        File file;
        for (String path : paths) {
            file = new File(path);
            if (file.exists()) {
                findClasses(file, file, true, visitor);
            }
        }
    }

    private static boolean findClasses(File root, File file, boolean includeJars, Visitor<Class> visitor) {
        final ClassLoader classLoader = ClassFinder.class.getClassLoader();
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                if (!findClasses(root, child, includeJars, visitor)) {
                    return false;
                }
            }
        } else {
            if (includeJars) {
                JarFile jar = null;
                try {
                    jar = new JarFile(file);
                } catch (Exception ignore) {}
                if (jar != null) {
                    Enumeration<JarEntry> entries = jar.entries();
                    while (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        String name = entry.getName();
                        int extIndex = name.lastIndexOf(".class");
                        if (extIndex > 0) {
                            try {
                                name = name.replace('/', '.').replace(".class", "");
                                if (!visitor.visit(classLoader.loadClass(name)))
                                    return false;
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (file.getName().toLowerCase().endsWith(".class")) {
                    String name = file.getPath();
                    try {
                        if (!visitor.visit(classLoader.loadClass(name)))
                            return false;
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return true;
    }

    //FIXME naive implementation - this should be bulletproof!
    public static void forEachClassIn(JarFile jar, ClassLoader classLoader, Visitor<Class> visitor) { // FIXME? this will load every single class in the jarfile
        Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String name = entry.getName();
            int extIndex = name.lastIndexOf(".class");
            if (extIndex > 0) {
                try {
                    name = name.replace('/', '.').replace(".class", "");
                    if (!visitor.visit(classLoader.loadClass(name)))
                        return;
                } catch (ClassNotFoundException ignore) {} //TODO this can not happen or can it?
            }
        }
    }
}