package org.dianati.runtime.core.web;

import org.dianati.runtime.core.bean.BeanRegistry;
import org.dianati.runtime.core.module.ModuleEntry;
import org.dianati.runtime.core.module.ModuleException;
import org.dianati.runtime.core.module.ModuleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

@Controller
public class MainWebController {

    private final ModuleManager manager;
    private final BeanRegistry beanRegistry;
    private final Runtime runtime = Runtime.getRuntime();
    private final int MB = 1024 * 1024;

    @Autowired
    public MainWebController(@Qualifier("ModuleManager") ModuleManager manager,
                             @Qualifier("MainBeanRegistry") BeanRegistry beanRegistry) {
        this.manager = manager;
        this.beanRegistry = beanRegistry;
    }

    @GetMapping("/")
    public String base() {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String home(@RequestParam(name = "ajax", defaultValue = "false") boolean ajax) {
        if (ajax)
            return "home :: body";
        return "base :: base(page='home', fragment='body', site='/home')";
    }

    @GetMapping("/console")
    public String console(@RequestParam(name = "ajax", defaultValue = "false") boolean ajax) {
        if (ajax)
            return "wip :: body";
        return "base :: base(page='wip', fragment='body', site='/console')";
    }

    @GetMapping("/about")
    public String about(@RequestParam(name = "ajax", defaultValue = "false") boolean ajax) {
        if (ajax)
            return "wip :: body";
        return "base :: base(page='wip', fragment='body', site='/about')";
    }

    @GetMapping("/status")
    public String status(Model model, @RequestParam(name = "ajax", defaultValue = "false") boolean ajax) {
        model.addAttribute("services", beanRegistry.getBeans());
        model.addAttribute("processors", runtime.availableProcessors());
        model.addAttribute("threads", Thread.activeCount());
        model.addAttribute("usedMemory", (runtime.totalMemory() - runtime.freeMemory()) / MB);
        model.addAttribute("freeMemory", (runtime.freeMemory() / MB));
        model.addAttribute("totalMemory", (runtime.totalMemory() / MB));
        model.addAttribute("maxMemory", (runtime.maxMemory() / MB));

        if (ajax)
            return "status :: body";
        return "base :: base(page='status', fragment='body', site='/status')";
    }

    @GetMapping("/modules")
    public String features(Model model, @RequestParam(name = "ajax", defaultValue = "false") boolean ajax) {
        final Map<Integer, ModuleEntry> modules = manager.getModules();
        model.addAttribute("modules", modules.values());
        if (ajax)
            return "modules :: body";
        return "base :: base(page='modules', fragment='body', site='/modules')";
    }

    @PostMapping("/modules")
    public String addModule(@RequestParam("moduleJar") MultipartFile jar) throws IOException {
        if (!jar.getOriginalFilename().endsWith(".jar")) // FIXME: 14.03.2018 Really basic test also create an error page
            return "redirect:/modules";

        final Path tempFile = Files.createTempFile(Paths.get("F:/"), "module", "-" + jar.getOriginalFilename());
        // FIXME: 15.03.2018 add default module store path
        Files.copy(jar.getInputStream(), tempFile, StandardCopyOption.REPLACE_EXISTING);
        manager.deployModule(tempFile.toFile());
        return "redirect:/modules";
    }

    @RequestMapping("/modules/{id}/remove")
    public String removeModule(@PathVariable("id") int id) throws ModuleException, IOException {
        Path path = manager.getModules().get(id).getLocation();
        manager.undeployModule(id);
        Files.delete(path);
        return "redirect:/modules";
    }

    @RequestMapping("/modules/{id}/start")
    public String startModule(@PathVariable("id") int id) throws ModuleException {
        manager.startModule(id);
        return "redirect:/modules";
    }

    @RequestMapping("/modules/{id}/stop")
    public String stopModule(@PathVariable("id") int id) throws ModuleException {
        manager.stopModule(id);
        return "redirect:/modules";
    }
}
