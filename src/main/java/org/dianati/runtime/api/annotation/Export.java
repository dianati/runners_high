package org.dianati.runtime.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * Exposes a Service or a package ??
 */
@Target(value = {PACKAGE})
@Retention(RetentionPolicy.CLASS)
public @interface Export {
}
