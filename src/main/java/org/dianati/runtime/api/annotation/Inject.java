package org.dianati.runtime.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(value = {TYPE, ANNOTATION_TYPE, PACKAGE, FIELD})
@Retention(value = RUNTIME)
public @interface Inject {
}
