package org.dianati.runtime.api.common;

public interface Logger {

    enum Level {
        WARNING,
        INFO,
        ERROR,
        DEBUG;

        @Override
        public String toString() {
            return name();
        }
    }

    void log(Level level, String message);
}
