package org.dianati.runtime.api.common;

public class StateChangeEvent {

    private final String actState;
    private final String prevState;
    private final String module;

    public StateChangeEvent(String actState, String prevState, String module) {
        this.actState = actState;
        this.prevState = prevState;
        this.module = module;
    }

    public String getActState() {
        return actState;
    }

    public String getPrevState() {
        return prevState;
    }

    public String getModule() {
        return module;
    }
}
