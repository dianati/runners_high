package org.dianati.runtime.api.common;

public interface Event<T> {

    void fire(T event);
}


