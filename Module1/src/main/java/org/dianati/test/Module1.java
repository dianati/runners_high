package org.dianati.test;

import org.dianati.runtime.api.annotation.Import;
import org.dianati.runtime.api.annotation.RunnersModule;
import org.dianati.runtime.api.annotation.Start;
import org.dianati.runtime.api.annotation.Stop;
import org.dianati.runtime.api.common.Event;
import org.dianati.runtime.api.common.Logger;
import org.dianati.runtime.api.common.StateChangeEvent;
import org.dianati.test.beans.SimpleTestClass;
import org.dianati.test.beans.TestBean;

@Import(modules = "Module_2") //import all exported packages from module_2
@RunnersModule(name = "Module_1")
public class Module1 {

    private final Event<StateChangeEvent> events;
    private final Logger logger;
    private final TestBean testBean;

    private final Object lock = new Object();
    private boolean run = true;

    public Module1(Logger logger, Event<StateChangeEvent> events, TestBean testBean) {
        this.logger = logger;
        this.events = events;
        this.testBean = testBean;
    }

    @Start
    public void start() {
        logger.log(Logger.Level.INFO, "Module1 start was called");
        events.fire(new StateChangeEvent("started", "deployed", "Module_1"));
        synchronized (lock) {
            while (run) {
                try {
                    Thread.sleep(5000);
                    System.out.println("Module_1 does some weird stuff!");
                    testBean.print(); //use a service from an import
                    new SimpleTestClass().print("test"); //use a class from an import
                } catch (InterruptedException ignore) {
                    System.out.println("FU!");
                }
            }
        }
    }

    @Stop
    public void stop() {
        logger.log(Logger.Level.INFO, "Module1 stop was called");
        run = false;
        synchronized (lock) {
            System.out.println("Module_2 finished!!");
            events.fire(new StateChangeEvent("deployed", "started", "Module_1"));
        }
    }
}
